<?php defined('SYSPATH') or die('No direct script access.');

Route::set('auth:login', 'auth/login')
    ->defaults([
        'directory'  => 'Client',
        'controller' => 'Auth',
        'action'     => 'login'
    ]);

Route::set('auth:finish', 'auth/finish')
    ->defaults([
        'directory'  => 'Client',
        'controller' => 'Auth',
        'action'     => 'finish',
    ]);

Route::set('auth:logout', 'auth/logout')
    ->defaults([
        'directory'  => 'Client',
        'controller' => 'Auth',
        'action'     => 'logout',
    ]);

Route::set('evaluation:student', 'student/evaluate')
    ->defaults([
        'directory'  => 'Client/EvaluationInterface',
        'controller' => 'Student',
        'action'     => 'index',
    ]);

Route::set('results', 'results')
    ->defaults([
        'directory'  => 'Client/ResultsInterface',
        'controller' => 'Results',
        'action'     => 'show',
    ]);

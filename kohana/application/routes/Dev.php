<?php

Route::set('dev:SOAP:call', 'dev/soap/call/<function>')
    ->defaults([
        'directory'  => 'Dev',
        'controller' => 'SOAP',
        'action'     => 'Call',
    ]);

Route::set('dev:Twig:show', 'dev/twig/show/<filepath>')
    ->defaults([
        'directory'  => 'Dev',
        'controller' => 'Twig',
        'action'     => 'Show',
    ]);
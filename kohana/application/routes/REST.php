<?php defined('SYSPATH') or die('No direct script access.');

// GET: Objects of generic
Route::set('GET::Generic', '<generic>/<id>(/<object>(.<format>))', [
//		'generic' => '(university|faculty|department|metric)',
    'generic' => '(university|faculty|metric)',
    'object'  => '(teachers|rating|list)',
    'format'  => '(json|xml)',
])
    ->filter(function (Route $route, $params, Request $request) {
        if ($request->method() !== HTTP_Request::GET)
            return false;
        if (empty($params['object']) && !empty($params['id']))
            $params['object'] = $params['id'];
        $params['controller'] = $params['generic'];
        $params['action'] = $params['object'];
        return $params;
    })
    ->defaults([
        'directory' => 'Server',
        'format'    => 'json'
    ]);

// GET: Teacher information
Route::set('GET::Teacher', 'teacher/<id>(/<object>(.<format>))', [
    'object' => '(evaluation|position|metrics|stat)',
    'format' => '(json|xml)'
])
    ->filter(function (Route $route, $params, Request $request) {
        if ($request->method() !== HTTP_Request::GET)
            return false;
        $params['action'] = 'get' . $params['object'];
        return $params;
    })
    ->defaults([
        'directory'  => 'Server',
        'controller' => 'Teacher',
        'object'     => 'evaluation',
        'format'     => 'json'
    ]);

// POST: Teacher information
Route::set('POST::Teacher', 'teacher/<id>/<action>', [
    'action' => '(evaluate)'
])
    ->filter(function (Route $route, $params, Request $request) {
        if ($request->method() !== HTTP_Request::POST)
            return false;
        return $params;
    })
    ->defaults([
        'directory'  => 'Server',
        'controller' => 'Teacher'
    ]);
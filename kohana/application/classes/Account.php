<?php defined('SYSPATH') or die('No direct script access.');

class Account {

	public static function signIn($signatureHash, $globalKey, $flags)
	{
		$session = Session::instance();
		$key = explode('-', $globalKey);
		$session->set('signatureHash', $signatureHash);
		$session->set('globalKey', Account::getUnifiedID($key[1]));
		$session->set('useragent', $_SERVER['HTTP_USER_AGENT']);
		$session->set('isStudent', $flags['isStudent']);
		$session->set('isStaff', $flags['isStaff']);

		return true;
	}

    /**
     * Проверяет авторизационный статус пользователя и, если
     * пользователь имеет UserAgent и IP, отличные от хранимых
     * в сессии, осуществляет выход из текущего сеанса.
     *
     * @return bool  true, если пользователь авторизован
     */
    public static function isSignedIn() {
		$session = Session::instance();
		$checkUserAgent =($_SERVER['HTTP_USER_AGENT'] !== $session->get('useragent'));
		if ($checkUserAgent){
			return !self::signOut();
		} else {
			return true;
		}
    }

	public static function getUnifiedID($source)
	{
		$pfx = '';
		for($i = 0; $i < 9 - strlen($source); $i++)
			$pfx .= '0';
		$id = $pfx.$source;
		return $id;
	}
	public static function isStudent() {
		return Session::instance()->get('isStudent');
	}

	public static function isStaff() {
		return Session::instance()->get('isStaff');
	}

    /**
     * Завершает текущий сеанс пользователя.
     * @return bool
     */
    public static function signOut() {
		$session = Session::instance();
		$session->restart();
		$session->destroy();
		$session->regenerate();
		return true;
    }

	public static function getAuthData() {
		$sHash = Session::instance()->get('signatureHash');
		$globalKey = Session::instance()->get('globalKey');
		$openKey = sha1($sHash);
		$token = sha1($openKey.Cookie::$salt.$globalKey);
		return ["Key" => $openKey, "Token" => $token, "ID" => $globalKey];
	}

	public static function checkAuthData($key, $token, $id)
	{
		return sha1($key.Cookie::$salt.$id) == $token;
	}
}
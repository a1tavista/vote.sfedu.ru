<?php defined('SYSPATH') or die('No direct script access.');

class Controller_REST extends Controller {

	/** @var  Validation */
	protected $post;

	/** @var  Validation */
	protected $get;

	/** @var  array */
	protected $result;

	public function before()
	{
		$this->post = Validation::factory(Arr::map('trim', $_POST));
		$this->get = Validation::factory(Arr::map('trim', $_GET));

		$this->post->offsetExists('Key') ?
			$auth = $this->post->data() : $this->get->data();

		if(!isset($auth['Key'])) exit;

		if(!Account::checkAuthData($auth['Key'], $auth['Token'], $auth['ID']))
		{
			echo "Wrong auth data";
			exit;
		}
	}

	public function after()
	{
		$type = $this->request->param('type', 'json');
        $result = 0;

        if ($type == 'jsonp') {
            if(!empty($_GET['callback'])) {
                $callback = $this->get['callback'];
                $result = $callback . '(' . json_encode($this->result, JSON_FORCE_OBJECT) . ');';
            }
        }
        elseif ($type == 'json')
            $result = is_array($this->result) ? json_encode($this->result) : $this->result;
        elseif ($type == 'xml')
            $result = is_array($this->result) ? xmlrpc_encode($this->result) : $this->result;
		$this->response->body($result);
	}
}

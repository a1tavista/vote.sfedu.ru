<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Server_Metric extends Controller_REST {

	public function action_teachers()
	{
        $id = $this->request->param('id');
        $this->result = Model_Metric::getTeachers($id);
	}

	public function action_rating()
	{
        $id = $this->request->param('id');
        $this->result = Model_Metric::getRating($id);
	}

	public function action_list()
	{
        $this->result = Model_Metric::getList();
	}

}
<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Server_Teacher extends Controller_REST {

	/**
	 * @return int
	 * @throws Kohana_Exception
	 *
	 * Действие "оценивание преподавателя". Возвращает пользователю следующие коды:
	 * -- 200 - OK
	 * -- 404 - Преподавателя нет в базе данных
	 * -- 403 - Преподавателя нет в списке для оценивания
	 * -- 406 - Преподаватель был оценен ранее
	 */
	public function action_evaluate()
	{
		// Извлечение параметров запроса
		$id = $this->request->param('id');
		$studentID = $this->post['ID'];
		$metricsArr = isset($this->post['Metrics']) ? $this->post['Metrics'] : [];

        // Инициализация модели
        $teacher = Model_Teacher::load($id, false);
		if(!$teacher) {
			$this->response->status(404);
			return -1;
		}

		// Проверяем, может ли студент оценивать данного преподавателя
		if(!$this->checkStudentAccess($studentID, $id))
		{
			$this->response->status(403);
			return -1;
		}

		// Проверяем, оценивал ли студент преподавателя ранее
		if(!$teacher->isEvaluatedByStudent($studentID))
		{
			$this->response->status(406);
            return -1;
		}

		// Заносим оценки студента для данного преподавателя в базу данных

		foreach($metricsArr as $metric) {
			if(!$metric) continue;
			$teacher->evaluate($metric);
		}

		$teacher->recordStudentEvaluation($studentID);

		$this->response->status(200);
        return 0;
	}

	public function action_getEvaluation()
	{
		$id = (int) $this->request->param('id');

        if(!$teacher = Model_Teacher::load($id, false))
            return -1;

        $this->result = [
				'Evaluation' => $teacher->averageEvaluation()
        ];
        return 0;
	}

	public function action_getPosition()
	{
        $id = (int) $this->request->param('id');
        if(!$teacher = Model_Teacher::load($id, false))
        {
            $this->result = 'No teacher found';
            $this->response->status(404);
            return -1;
        }

        $this->result = [
				'ByDepartment' => $teacher->departmentRatingPosition(),
				'ByFaculty' => $teacher->facultyRatingPosition(),
				'ByUniversity' => $teacher->universityRatingPosition(),
        ];
        return 0;
	}

	public function action_getMetrics()
	{
        $id = (int) $this->request->param('id');
		$teacher = Model_Teacher::load($id, false);
//        $this->result = $teacher->averageByMetrics();
	}

	public function checkStudentAccess($studentID, $teacherID)
	{
		$teachers = Model_SOAP_Teachers::forStudent($studentID);
		foreach($teachers as $teacher)
			if($teacher['prep_kod'] === $teacherID)
				return true;
		return false;
	}

}

<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Server_Faculty extends Controller_REST {

	public function action_teachers()
	{
        $id = $this->request->param('id');
        $this->result = Model_Faculty::getTeachers($id);
	}

	public function action_rating()
	{
        $id = $this->request->param('id');
        $this->result = Model_Faculty::getRating($id);
	}

	public function action_list()
	{
        $this->result = Model_Faculty::getList();
	}

}

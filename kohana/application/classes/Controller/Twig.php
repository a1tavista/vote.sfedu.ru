<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Twig extends Controller {
    public function action_show()
    {
        $id = $this->request->param('id');
        $path = UTF8::str_ireplace(':', '/', $id);
        if(Kohana::find_file('views', $path, 'twig'))
        {
            $twig = Twig::factory($path);
            $this->response->body($twig);
        }
        else
        {
            throw HTTP_Exception::factory (404, 'Искомый шаблон не найден');
        }
    }
}

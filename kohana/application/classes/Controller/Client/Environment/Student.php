<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Client_Environment_Student extends Controller_Client_Environment {
    public function before()
    {
        if(!Account::isStudent())
        {
            $this->redirect('/', 302);
            return;
        }
    }

    public function after()
    {

    }
}

<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Client_EvaluationInterface_Student extends Controller_Client_Environment_Student
{
    public function action_index() {
        $twig = Twig::factory('evaluationInterface/student');
        $studentID = Session::instance()->get('globalKey');

        $allTeachers = Model_SOAP_Teachers::forStudent($studentID);
        $evaluatedTeachers = Model_Student::getTeachers($studentID);
        list($groupedByYears, $evaluated) = $this->prepareTeachers($allTeachers, $evaluatedTeachers);

        $twig->User = Account::getAuthData();
        $twig->Metrics = Model_Metric::getList();
        $twig->set(['Teachers' => $groupedByYears, 'Evaluated' => $evaluated]);
        $this->response->body($twig);
    }

    protected function prepareTeachers($teachers, $evaluated)
    {
        $handled = $evHandled = [];

        foreach($evaluated as $ev)
        {
            $handled_evaluated[$ev['ExternalID']] = 1;
        }

        foreach ($teachers as $val) {
            // Group by year
            $year = [];
            if(isset($val['edu_year']['Edu_year_name'])) {
                $yr = explode('-', $val['edu_year']['Edu_year_name']);
                $year[] = trim($yr[0]);
            }
            else {
                foreach ($val['edu_year'] as $yr) {
                    $yrr = explode('-', $yr['Edu_year_name']);
                    $year[] = trim($yrr[0]);
                }
            }

            foreach($year as $yr) {
                $handled[$yr][$val['prep_kod']] = [
                    'ID' => $val['prep_kod'],
                    'Name' => $val['prep_fio'],
                    'IsEvaluated' => ($isEv = isset($handled_evaluated[$val['prep_kod']])) ? '1' : '0'
                ];
                if ($isEv)
                    $evHandled[$val['prep_kod']] = $handled[$yr][$val['prep_kod']];
            }
        }

        return [$handled, $evHandled];
    }
}

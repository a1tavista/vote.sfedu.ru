<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Client_Auth extends Controller
{
    private $twig, $redirect_url;

    public function before()
    {
        $this->twig = Twig::factory('access');
    }

    public function action_logout()
    {
        Account::signOut();
        $this->twig->Message = 'До скорых встреч!';
        $this->twig->Description = 'Вы будете перенаправлены на главную страницу через 3 секунды.';
        $this->redirect_url = Route::url('main:landing');
    }

    public function action_login()
    {
        $openid = new OpenID;
        $openid->SetIdentity("https://openid.sfedu.ru/server.php/idpage?user=" . $_GET["user"]);
        $openid->SetTrustRoot('http://' . $_SERVER["HTTP_HOST"]);
        $openid->SetOptionalFields(['email', 'nickname', 'r61globalkey', 'staff', 'student']);
        if ($openid->GetOpenIDServer()) {
            $openid->SetApprovedURL('http://' . $_SERVER["HTTP_HOST"] . '/auth/finish');
            $openid->Redirect();
        } else {
            $error = $openid->GetError();
            echo "ERROR CODE: " . $error['code'] . "<br>";
            echo "ERROR DESCRIPTION: " . $error['description'] . "<br>";
        }
    }

    public function action_finish()
    {
        if ($_GET["openid_sreg_student"] !== '1') {
            $this->twig->Message = 'Упс!';
            $this->twig->Description = 'К сожалению, личный кабинет сотрудника пока что находится в разработке. Следите за новостями!';
            $this->redirect_url = Route::url('main:landing');
        }

        if ($_GET['openid_mode'] == 'id_res') {
            $openid = new OpenID;
            $openid->SetIdentity($_GET['openid_identity']);
            $openid_validation_result = $openid->ValidateWithServer();

            if ($openid_validation_result == true) {
                $this->signIn();
            } else if ($openid->IsError() == true) {
                $error = $openid->GetError();
                $this->twig->Message = 'Ошибка!';
                $this->twig->Description = "[" . $error['code'] . "]: " . $error['description'];
            } else {
                $this->twig->Message = 'Ошибка!';
                $this->twig->Description = "При авторизации что-то пошло не так. Попробуете снова?";
            }
        } else if ($_GET['openid_mode'] == 'cancel') {
            $this->twig->Message = 'Какая досада!';
            $this->twig->Description = "Вы досрочно прекратили процесс авторизации. Надеемся, что у Вас всё хорошо.";
        }
    }

    private function signIn()
    {
        $flags = [
            'isStudent' => $_GET["openid_sreg_student"],
            'isStaff' => $_GET["openid_sreg_staff"]
        ];

        $this->twig->Message = 'Добро пожаловать!';
        $this->twig->Description = 'Вы будете перенаправлены на Вашу страницу через 3 секунды.';
        $this->redirect_url = Route::url('evaluation:student');

        Account::signIn($_GET["openid_sig"], $_GET["openid_sreg_r61globalkey"], $flags);
    }

    public function after()
    {
        $this->response->body($this->twig);
        header('Refresh: 3; URL=' . $this->redirect_url);
    }
}
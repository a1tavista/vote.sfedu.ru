<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Client_ResultsInterface_Results extends Controller_Twig
{
    public function action_show() {
        $twig = Twig::factory('resultsInterface/rating');
        $twig->Metrics = Model_Metric::getList();
        $twig->Faculties = Model_Faculty::getList();
        $this->response->body($twig);
    }
}
<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Client_LandingPage_Index extends Controller_Twig
{
    public function action_landing() {
        $twig = Twig::factory('landingPage/index');
        $twig->Metrics = Model_Metric::getList();
        $this->response->body($twig);
    }
}

<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Client_Environment extends Controller_Twig {
    public function before()
    {
        if(!Account::isSignedIn())
        {
            $this->redirect('/', 302);
            return;
        }
    }

    public function after()
    {
        // TODO: Отображение страницы
    }
}

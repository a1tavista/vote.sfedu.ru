<?php

/**
 *    Class Model_Metric
 *
 * @property $ID                      int
 * @property $FacultyID               int
 * @property $DepartmentID            int
 */
class Model_Metric extends Model
{
    public static function getList() {
        $sql = 'CALL Metric_GetList()';
        /** @var Database_Result $result */
        $result = DB::query(Database::SELECT, $sql)->execute();
        return $result->as_array();
    }

    public static function getTeachers($id) {
        $sql = 'CALL Metric_GetTeachers(:id)';
        /** @var Database_Result $result */
        $result = DB::query(Database::SELECT, $sql)
                    ->param(':id', (int) $id)
                    ->execute();
        return $result->as_array();
    }

    public static function getRating($id) {
        $sql = 'CALL Metric_GetRating(:id, NULL)';
        /** @var Database_Result $result */
        $result = DB::query(Database::SELECT, $sql)
                    ->param(':id', (int) $id)
                    ->execute();
        return $result->as_array();
    }
}
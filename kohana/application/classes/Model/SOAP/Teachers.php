<?php

class Model_SOAP_Teachers extends Model
{
    public static function forStudent($id)
    {
        $result = SOAP_Request::instance()
            ->call("GetStudPreps", ['StudentID' => $id])
            ->asArray("STUD_PREPS");
        return $result['prep'];
    }
}
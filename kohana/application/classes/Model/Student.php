<?php

class Model_Student extends Model
{
    public static function getTeachers($studentID, $semesterID = null) {
        $sql = 'CALL Student_GetEvaluations(:id, :sem)';
                $result = DB::query(Database::SELECT, $sql)
                    ->param(':id', $studentID)
                    ->param(':sem', $semesterID);
        return $result->execute()->as_array();
    }
}
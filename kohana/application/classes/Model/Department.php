<?php

/**
 *    Class Model_Department
 *
 * @property $ID                      int
 * @property $FacultyID               int
 * @property $DepartmentID            int
 */
class Model_Department extends Model
{
    public static function getList() {
        $sql = 'CALL Department_GetList()';
        /** @var Database_Result $result */
        $result = DB::query(Database::SELECT, $sql)->execute();
        return $result->as_array();
    }

    public static function getTeachers($id) {
        $sql = 'CALL Department_GetTeachers(:id)';
        /** @var Database_Result $result */
        $result = DB::query(Database::SELECT, $sql)
            ->param(':id', (int) $id)
            ->execute();
        return $result->as_array();
    }

    public static function getRating($id) {
        $sql = 'CALL Department_GetRating(:id)';
        /** @var Database_Result $result */
        $result = DB::query(Database::SELECT, $sql)
            ->param(':id', (int) $id)
            ->execute();
        return $result->as_array();
    }
}
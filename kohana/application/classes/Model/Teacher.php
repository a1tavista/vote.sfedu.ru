<?php

/**
 *    Class Model_Teacher
 *
 * @property $ID                      int
 * @property $InternalID              int
 * @property $FacultyID               int
 * @property $DepartmentID            int
 */
class Model_Teacher extends Model_Container
{

    protected function create() { }

    protected function getRawData($id) {
        $id = Account::getUnifiedID($id);
        $sql = "CALL Teacher_GetInfo(:id)";
        $result = DB::query(Database::SELECT, $sql)->param(':id', $id)->execute()->as_array();
        if (!$result)
            throw new InvalidArgumentException('Teacher #'.$id.' not found');
        return $result[0];
    }

    /**
     *    Evaluate teacher by student
     *
     * @param    $Metrics       array    Linear array with metric IDs
     * @param    $SemesterID    int
     * @return $this
     */
    public function evaluate($MetricID, $SemesterID = null) {
        $sql = 'SELECT Teacher_Evaluate(:id, :metric, :semester) AS Result';
        DB::query(Database::SELECT, $sql)
            ->param(':id', $this->ID)
            ->param(':metric', $MetricID)
            ->param(':semester', $SemesterID)->execute();
        return $this;
    }

    public function recordStudentEvaluation($StudentID, $SemesterID = null) {
        $sql = 'SELECT Teacher_RecordStudentEvaluation(:id, :student, :semester) AS Result';
        DB::query(Database::SELECT, $sql)
            ->param(':id', $this->ID)
            ->param(':student', $StudentID)
            ->param(':semester', $SemesterID)->execute();
        return $this;
    }

    public static function createTeacher($ID, $FacultyID, $DepartmentID) {
        $sql = 'SELECT Teacher_Create(:id, :faculty, :department) AS Result';
        $result = DB::query(Database::SELECT, $sql)
            ->param(':id', $ID)
            ->param(':faculty', $FacultyID)
            ->param(':department', $DepartmentID)->execute()->as_array();
        return $result[0];
    }

    public function getMetrics($SemesterID = null) {
        $sql = "CALL Teacher_GetMetrics(:id, :semester)";
        $result = DB::query(Database::SELECT, $sql)
            ->param(':id', $this->ID)
            ->param(':semester', $SemesterID)->execute()->as_array();
        return $result;
    }

    /**
     *    Checks that teacher is already evaluated by student
     *
     * @param    $StudentID     int
     * @param    $SemesterID    int
     * @return true if evaluated, false otherwise
     */
    public function isEvaluatedByStudent($StudentID, $SemesterID = null) {
        $sql = 'SELECT Teacher_IsEvaluated(:id, :student, :semester) AS Result';
        $res = DB::query(Database::SELECT, $sql)
            ->param(':id', $this->ID)
            ->param(':student', $StudentID)
            ->param(':semester', $SemesterID)->execute()->as_array();
        return ((int) $res[0]['Result']) == 0;
    }

    /**
     *    ---
     *
     * @param    $SemesterID    int
     * @return float    Evaluation
     */
    public function averageEvaluation($SemesterID = null) {
        $sql = 'SELECT Teacher_GetEvaluation(:id, :semester) AS Result';
        return DB::query(Database::SELECT, $sql)
            ->param(':id', $this->ID)
            ->param(':semester', $SemesterID)->execute()->as_array()
            ->groupByUniqueKey('Result');
    }

    /**
     *    ---
     *
     * @param    $SemesterID    int
     * @return float    Evaluation
     */
    public function facultyRatingPosition($SemesterID = null) {
        $sql = 'SELECT Teacher_GetPositionByFaculty(:id, :semester) AS Result';
        $res = DB::query(Database::SELECT, $sql)
            ->param(':id', $this->ID)
            ->param(':semester', $SemesterID)->execute()->as_array();
        return $res[0]['Result'];
    }

    /**
     *    ---
     *
     * @param    $SemesterID    int
     * @return float    Evaluation
     */
    public function universityRatingPosition($SemesterID = null) {
        $sql = 'SELECT Teacher_GetPositionByUniversity(:id, :semester) AS Result';
        $res = DB::query(Database::SELECT, $sql)
            ->param(':id', $this->ID)
            ->param(':semester', $SemesterID)->execute()->as_array();
        return $res[0]['Result'];
    }

    /**
     *    ---
     *
     * @param    $SemesterID    int
     * @return float    Evaluation
     */
    public function departmentRatingPosition($SemesterID = null) {
        $sql = 'SELECT Teacher_GetPositionByDepartment(:id, :semester) AS Result';
        $res = DB::query(Database::SELECT, $sql)
            ->param(':id', $this->ID)
            ->param(':semester', $SemesterID)->execute()->as_array();
        return $res[0]['Result'];
    }
}
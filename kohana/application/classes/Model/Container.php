<?php

/**
 * @property-read $ID int
 */
abstract class Model_Container extends Model implements JsonSerializable, ArrayAccess
{
    /**
     * A case-sensitive string for ID field.
     * The field must exist in every container.
     * @var string
     */
    protected static $ID_FIELD = 'ID';

    /**
     * A flag for builders to create new instances in db.
     * Used in Helper/Builder.php
     */
    private static $CREATE_FLAG = '__CREATE__!@#$%^&;';


    protected $data;

    /**
     * @var bool
     * Indicates whether data was loaded from db.
     */
    protected $loaded = false;


    /**
     * @param $data        array
     *                     Source data for building the object.
     * @param $associated  bool
     *                     Indicates whether object exists in db.
     * @param $lazyLoading bool
     *                     Indicates whether lazy loading can be used.
     */
    public function __construct(array & $data, $associated = true, $lazyLoading = true) {
        $this->data = $data;

        if (!$associated && $data[self::$CREATE_FLAG]) {
            unset($this->data[self::$CREATE_FLAG]);
            $this->create();
        }

        if (!isset($this->data[self::$ID_FIELD]))
            throw new LogicException("ID is incorrect");

        if(!$lazyLoading)
            $this->getRawDataInLazyMode();
    }

    /**
     * Create new object in db, based on $this->data.
     * This function is called from constructor.
     */
    protected abstract function create();


    /**
     * Creation of new object from a raw data.
     * Redefine it and call any of builders.
     */
    public static function make() { }


    /**
     * Load the object from database.
     * @param $id int  object id
     * @return $this  new instance
     */
    public static function load($id, $lazy = true) {
        if (!is_numeric($id) || $id <= 0)
            throw new InvalidArgumentException("ID is incorrect!");

        // lazy loading
        $data[self::$ID_FIELD] = (int) $id;
        return new static($data, true, $lazy);
    }

    /**
     * @param $id int object id
     * @return array data from database
     */
    protected abstract function getRawData($id);

    protected function getRawDataInLazyMode() {
        if (!$this->loaded) {
            $this->data = array_merge($this->data, static::getRawData($this->ID));
            $this->loaded = true;
        }
    }

    public function exists() {
        try {
            $this->getRawDataInLazyMode();
        } catch (InvalidArgumentException $e) { }
        return $this->loaded;
    }

    /*
     * Getters & setters.
     */

    public function __get($name) {
        // if value has already been loaded
        if (array_key_exists($name, $this->data))
            return $this->data[$name];

        // if no, then make query to db
        $this->getRawDataInLazyMode();
        if (array_key_exists($name, $this->data))
            return $this->data[$name];

        throw new ErrorException('No such field or property');
    }

    public function __isset($name) {
        return array_key_exists($name, $this->data);
    }

    public function __set($name, $value) {
        if ($name == self::$ID_FIELD)
            throw new BadMethodCallException();
        $this->data[$name] = $value;
    }

    // todo: remove this one
    public function offsetExists($name) {
        $name = ucfirst($name);

        if (array_key_exists($name, $this->data))
            return true;

        $this->getRawDataInLazyMode();
        return array_key_exists($name, $this->data);
    }

    public function offsetGet($offset) {
        return $this->__get($offset);
    }

    public function offsetSet($offset, $value) {
        $this->__set($offset, $value);
    }

    public function offsetUnset($offset) {
        throw new BadMethodCallException();
        // this method may be confused by lazy loading
    }

    /** @deprecated */
    public function toArray() {
        $this->getRawDataInLazyMode();
        return $this->data;
    }

    public function jsonSerialize($forceLoad = true) {
        if ($forceLoad)
            $this->getRawDataInLazyMode();
        return $this->data;
    }
}
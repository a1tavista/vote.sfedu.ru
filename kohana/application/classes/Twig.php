<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Twig
 *
 * @property $HTML HTML
 * @property $Date Date
 * @property $URL URL
 * @property $UTF8 UTF8
 * @property $Text Text
 * @property $Rus RusLang
 * @property $Arr Arr
 * @property $Inflector Inflector
 * @property $System array[]
 */
class Twig extends Kohana_Twig
{
    public static function factory($file = NULL, array $data = NULL) {
        $twig = parent::factory($file, $data);
        $twig->set_global([
            'HTML' => new HTML,
            'Date' => new Date,
            'URL' => new URL,
            'UTF8' => new UTF8,
            'Text' => new Text,
            'Inflector' => new Inflector,
            'Route' => new Route,
            'Account' => new Account
        ]);
        return $twig;
    }
}
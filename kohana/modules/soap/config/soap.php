<?php defined('SYSPATH') or die('No direct script access.');

return [

    // Сервер
    'uri' => '',


    // Учетка для подключения к серверу
    'connection' => [
        'login' => '',
        'password' => '',
        'trace' => true,
        'exceptions' => true,
        'cache_wsdl' => WSDL_CACHE_NONE
    ],

    // Опции, которые нужны в каждом запрсе. Да, login и pass указывать в каждом запросе
    'options' => [
        "login" => '',
        "password" => '',
        "soapaction" => '',
        "timeout" => '100000',
    ],

];

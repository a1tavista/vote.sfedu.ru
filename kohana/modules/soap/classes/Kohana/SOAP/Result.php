<?php defined('SYSPATH') OR die('No direct script access.');

class Kohana_SOAP_Result
{
    private $return;

    public function __construct($raw) {
        $this->return = $raw->return;
    }

    public function result($xmlElement)
    {
        if (isset($this->return->{$xmlElement}))
            return json_decode(json_encode($this->return->{$xmlElement}, JSON_UNESCAPED_UNICODE), true);
        else throw new SOAP_Exception();
    }

    public function asArray($xmlElement) {
        return $this->result($xmlElement);
    }

    public function asJSON($xmlElement) {
        return json_encode($this->result($xmlElement));
    }
}

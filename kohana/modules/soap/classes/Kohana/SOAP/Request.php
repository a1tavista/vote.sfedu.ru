<?php defined('SYSPATH') OR die('No direct script access.');

class Kohana_SOAP_Request implements ArrayAccess
{
    /** @var array params */
    private $queryParams;

    /** @var  self */
    protected static $_instance;

    private $_config;
    /** @var SoapClient _connect */
    private $_connect;

    public static function instance() {
        if (!isset(self::$_instance)) {
            $config = Kohana::$config->load('soap');
            self::$_instance = new self($config);
        }
        return self::$_instance;
    }

    private function __construct($config = []) {
        $opts = [
            'http' => [
                'user_agent' => 'PHPSoapClient'
            ]
        ];
        $context = stream_context_create($opts);
        $connection = $this->_config['connection'];
        $connection['stream_context'] = $context;
        $this->_config = $config;
        $this->_connect = new SoapClient($this->_config['uri'], $connection);
    }


    public function offsetExists($offset) {
        return isset($this->queryParams[$offset]);
    }

    public function offsetGet($offset) {
        return $this->offsetExists($offset) ? $this->queryParams[$offset] : false;
    }

    public function offsetSet($offset, $value) {
        $this->queryParams[$offset] = $value;
    }

    public function offsetUnset($offset) {
        if (isset($this->queryParams[$offset]))
            unset($this->queryParams[$offset]);
    }

    /**
     * Задает параметры запроса
     * @param array $soapParams
     * @return $this
     */
    public function params(array $soapParams) {
        $this->queryParams = isset($soapParams) ? $soapParams : [];
        return $this;
    }

    /**
     * Задает параметры запроса
     * @param string $soapAction
     * @return SOAP_Result
     */
    public function call($soapAction, array $soapParams) {
        try {
            $result = $this->_connect->__soapCall($soapAction, ['parameters' => $soapParams], $this->_config['options']);
            return new SOAP_Result($result);
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
            return false;
        }
    }
}

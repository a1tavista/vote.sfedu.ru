SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `teachers`
(
  ID         INT PRIMARY KEY    NOT NULL AUTO_INCREMENT,
  ExternalID VARCHAR(255)
             CHARACTER SET utf8 NOT NULL UNIQUE, -- ID преподавателя в 1С
  FacultyID  INT                NOT NULL, -- ID подразделения
  FirstName  VARCHAR(255)
             CHARACTER SET utf8 NOT NULL,
  SecondName VARCHAR(255)
             CHARACTER SET utf8 NOT NULL,
  LastName   VARCHAR(255)
             CHARACTER SET utf8 NOT NULL,
  Birthday   DATE               NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `faculties`
(
  ID    INT PRIMARY KEY    NOT NULL AUTO_INCREMENT,
  Code  VARCHAR(255)
        CHARACTER SET utf8 UNIQUE,
  Title VARCHAR(255)
        CHARACTER SET utf8 NOT NULL UNIQUE
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `accumulation_teachers`
(
  ID         INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  TeacherID  INT             NOT NULL, -- внутренний ID преподавателя
  SemesterID INT             NOT NULL, -- ID семестра
  Rating     FLOAT           NOT NULL -- средний балл преподавателя в данном семестре
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS metrics
(
  ID          INT PRIMARY KEY                  NOT NULL AUTO_INCREMENT,
  Title       VARCHAR(255)                     NOT NULL, -- Title - именование критерия
  Description TEXT
              CHARACTER SET utf8               NOT NULL,
  IsVisible   CHAR(4)
              CHARACTER SET utf8 DEFAULT 'yes' NOT NULL  -- IsVisible - виден ли критерий при голосовании
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS evaluations
(
  ID         INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  TeacherID  INT             NOT NULL, -- TeacherID - внутренний ID преподавателя
  MetricID   INT             NOT NULL, -- MetricID - ID метрики
  SemesterID INT             NOT NULL, -- SemesterID - ID семестра
  Votes      INT             NOT NULL, -- Количество лайков по критерию MetricID
  Rating     FLOAT           NOT NULL  -- Средний балл в рейтинге по критерию MetricID
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS triplets
(
  ID         INT PRIMARY KEY    NOT NULL AUTO_INCREMENT,
  TeacherID  INT                NOT NULL,
  StudentID  VARCHAR(255)
             CHARACTER SET utf8 NOT NULL,
  SemesterID INT                NOT NULL,
  EvalDate   DATE               NOT NULL,
  UNIQUE (TeacherID, StudentID, SemesterID)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS semesters
(
  ID      INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  SemYear YEAR            NOT NULL,
  SemNum  INT             NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
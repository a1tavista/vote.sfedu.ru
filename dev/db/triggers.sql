DELIMITER //

drop trigger if exists tr_u_avrEvaluation//

create trigger tr_u_avrEvaluation
  before insert on triplets for each row
begin

  -- 0. Объявляем счетчик студентов
  DECLARE StudentsCount INT;
  DECLARE MetricsCount INT;
  DECLARE ev FLOAT;

  -- 1. Получаем количество студентов, проголосовавших в данном семестре за данного преподавателя
  SELECT COUNT(ID) FROM triplets WHERE TeacherID = NEW.TeacherID AND SemesterID = NEW.SemesterID INTO StudentsCount;

  -- 2. Подсчитываем отношение количества проголосовавших по критерию к проголосовавшим за данного преподавателя
  -- См.: vk.cc/4qcqNw (Wikipedia)
  UPDATE evaluations SET evaluations.Rating = Utility_CalcMetricRating(evaluations.Votes, StudentsCount + 1)
    WHERE evaluations.TeacherID = NEW.TeacherID AND evaluations.SemesterID = NEW.SemesterID;

  -- 3. Заносим количество критериев в переменную cnt
  SELECT COUNT(ID) FROM metrics WHERE IsVisible = 'yes' INTO MetricsCount;

  -- 4. Обновляем общую оценку для преподавателя
  SELECT SUM(Rating) FROM evaluations WHERE evaluations.TeacherID = NEW.TeacherID AND evaluations.SemesterID = NEW.SemesterID INTO ev;

  UPDATE accumulation_teachers SET accumulation_teachers.Rating = (ev / MetricsCount)
      WHERE accumulation_teachers.TeacherID = NEW.TeacherID AND accumulation_teachers.SemesterID = NEW.SemesterID;
end//

DELIMITER ;
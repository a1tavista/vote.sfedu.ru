SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

DELIMITER //
-- ---------------------------------------------------------------------------------------------------------------------
-- Student_GetEvaluations(pStudentID, pExternalID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS Student_GetEvaluations//
CREATE PROCEDURE `Student_GetEvaluations`(
  `pStudentID` VARCHAR(255) CHARACTER SET utf8,
  `pSemesterID` INT
)
NO SQL
  BEGIN
    IF pSemesterID IS NULL THEN SELECT Semester_GetCurrent() INTO pSemesterID; END IF;
    SELECT triplets.TeacherID, teachers.ExternalID FROM triplets
      INNER JOIN teachers ON triplets.TeacherID = teachers.ID
      WHERE triplets.StudentID = pStudentID AND triplets.SemesterID = pSemesterID;
  END //

DELIMITER ;
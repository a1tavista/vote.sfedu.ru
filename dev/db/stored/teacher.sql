SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

DELIMITER //
-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_GetInternalID(pExternalID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_GetInternalID//
CREATE FUNCTION `Teacher_GetInternalID`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8
)
  RETURNS INT(11)
NO SQL
  BEGIN
    DECLARE result INT;

    SELECT teachers.ID FROM teachers
    WHERE teachers.ExternalID = pExternalTeacherID
    INTO result;

    RETURN result;
  END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_Evaluate(pExternalTeacherID, pMetricID, pSemesterID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_Evaluate//
CREATE FUNCTION `Teacher_Evaluate`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8,
  `pMetricID`   INT,
  `pSemesterID` INT
)
  RETURNS INT(11)
NO SQL
BEGIN
  DECLARE tid INT;
  DECLARE cnt INT;
  SELECT Teacher_GetInternalID(pExternalTeacherID) INTO tid;
  IF pSemesterID IS NULL THEN SELECT Semester_GetCurrent() INTO pSemesterID; END IF;
  SELECT COUNT(ID) FROM evaluations WHERE evaluations.TeacherID = tid AND evaluations.MetricID = pMetricID AND evaluations.SemesterID = pSemesterID INTO cnt;
  IF cnt = 0 THEN
    INSERT INTO evaluations (ID, TeacherID, MetricID, SemesterID, Votes, Rating)
                VALUES (NULL, tid, pMetricID, pSemesterID, 1, 5.0);
  ELSE
    UPDATE evaluations SET evaluations.Votes = evaluations.Votes + 1 WHERE evaluations.MetricID = pMetricID AND evaluations.TeacherID = tid AND evaluations.SemesterID = pSemesterID;
  END IF;
  RETURN 0;
END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_RecordStudentEvaluation(pExternalTeacherID, pStudentID, pSemesterID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_RecordStudentEvaluation//
CREATE FUNCTION `Teacher_RecordStudentEvaluation`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8,
  `pStudentID`   VARCHAR(255) CHARACTER SET utf8,
  `pSemesterID` INT
)
  RETURNS INT(11)
NO SQL
BEGIN
  DECLARE tid INT;
  IF pSemesterID IS NULL THEN SELECT Semester_GetCurrent() INTO pSemesterID; END IF;
  SELECT Teacher_GetInternalID(pExternalTeacherID) INTO tid;
  -- Фиксируем факт того, что студент проголосовал за преподавателя в данном семестре
  INSERT INTO triplets (ID, TeacherID, StudentID, SemesterID, EvalDate) VALUES (NULL, tid, pStudentID, pSemesterID, NOW());
  RETURN 0;
END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_IsExists(pExternalTeacherID, pStudentID, pSemesterID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_IsExists//
CREATE FUNCTION `Teacher_IsExists`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8
)
  RETURNS INT(11)
NO SQL
  BEGIN
    DECLARE result INT;
    SELECT ID FROM teachers WHERE teachers.ExternalID = pExternalTeacherID INTO result;
    RETURN result;
  END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_Create(pExternalTeacherID, pFacultyID, pDepartmentID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_Create//
CREATE FUNCTION `Teacher_Create`(
  `pLastName` VARCHAR(255) CHARACTER SET utf8,
  `pFirstName` VARCHAR(255) CHARACTER SET utf8,
  `pSecondName` VARCHAR(255) CHARACTER SET utf8,
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8,
  `pBirthday` DATE,
  `pFacultyCode` VARCHAR(255) CHARACTER SET utf8
)
  RETURNS INT(11)
NO SQL
  BEGIN
    IF Teacher_IsExists(pExternalTeacherID) THEN RETURN 0; END IF;
    INSERT INTO teachers
        (LastName, FirstName, SecondName, Birthday, ExternalID, FacultyID)
        VALUES
        (pLastName, pFirstName, pSecondName, pBirthday, pExternalTeacherID, Faculty_GetIDByCode(pFacultyCode));
    INSERT INTO accumulation_teachers (ID, TeacherID, SemesterID) VALUES (NULL, LAST_INSERT_ID(), Semester_GetCurrent());
    RETURN 1;
  END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_GetInfo(pExternalTeacherID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS Teacher_GetInfo//
CREATE PROCEDURE `Teacher_GetInfo`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8
)
NO SQL
BEGIN
  IF NOT Teacher_IsExists(pExternalTeacherID) THEN SELECT 0; END IF;
  SELECT
    ExternalID AS `ID`,
    ID AS `InternalID`,
    FacultyID,
    FirstName,
    SecondName,
    LastName,
    Birthday
  FROM teachers WHERE ExternalID = pExternalTeacherID;
END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_IsEvaluated(pExternalTeacherID, pStudentID, pSemesterID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_IsEvaluated//
CREATE FUNCTION `Teacher_IsEvaluated`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8,
  `pStudentID`  VARCHAR(255) CHARACTER SET utf8,
  `pSemesterID` INT
)
  RETURNS INT(11)
NO SQL
  BEGIN
    DECLARE result INT;
    IF pSemesterID IS NULL THEN SELECT Semester_GetCurrent() INTO pSemesterID; END IF;
    SELECT ID FROM triplets
      WHERE
        triplets.TeacherID = Teacher_GetInternalID(pExternalTeacherID)
        AND triplets.StudentID = pStudentID
        AND triplets.SemesterID = pSemesterID
      INTO result;
    RETURN result;
  END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_GetRecordsCount(pExternalTeacherID, pSemesterID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_GetRecordsCount//
CREATE FUNCTION `Teacher_GetRecordsCount`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8,
  `pSemesterID` INT
)
  RETURNS INT(11)
NO SQL
  BEGIN
    DECLARE result INT;
    IF pSemesterID IS NULL THEN SELECT Semester_GetCurrent() INTO pSemesterID; END IF;
    SELECT COUNT(ID) + 0.0 FROM triplets WHERE TeacherID = Teacher_GetInternalID(pExternalTeacherID) AND SemesterID = pSemesterID INTO result;
    RETURN result;
  END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_GetMetrics(pExternalTeacherID, pSemesterID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS Teacher_GetMetrics//
CREATE PROCEDURE `Teacher_GetMetrics`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8,
  `pSemesterID` INT
)
NO SQL
  BEGIN
    IF pSemesterID IS NULL THEN SELECT Semester_GetCurrent() INTO pSemesterID; END IF;

    SELECT metrics.ID,
           metrics.Title AS `Title`,
           evaluations.Rating AS `Rating`,
           evaluations.Votes AS `MetricVotes`,
           Teacher_GetRecordsCount(pExternalTeacherID, pSemesterID) AS `TotalVotes`
    FROM metrics
    INNER JOIN evaluations ON evaluations.MetricID = metrics.ID
    WHERE evaluations.TeacherID = Teacher_GetInternalID(pExternalTeacherID) AND evaluations.SemesterID = pSemesterID
    ORDER BY metrics.ID ASC;
  END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_GetEvaluation(pExternalTeacherID, pSemesterID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_GetEvaluation//
CREATE FUNCTION `Teacher_GetEvaluation`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8,
  `pSemesterID` INT
)
  RETURNS INT(11)
NO SQL
  BEGIN
    DECLARE result INT;
    IF pSemesterID IS NULL THEN SELECT Semester_GetCurrent() INTO pSemesterID; END IF;
    SELECT accumulation_teachers.Rating FROM accumulation_teachers
    WHERE
      accumulation_teachers.TeacherID = Teacher_GetInternalID(pExternalTeacherID)
      AND accumulation_teachers.SemesterID = pSemesterID
    INTO result;
    RETURN result;
  END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_GetPositionByFaculty(pExternalTeacherID, pSemesterID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_GetPositionByFaculty//
CREATE FUNCTION `Teacher_GetPositionByFaculty`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8,
  `pSemesterID` INT
)
  RETURNS INT(11)
NO SQL
  BEGIN
    DECLARE result INT;
    DECLARE fid INT;
    IF pSemesterID IS NULL THEN SELECT Semester_GetCurrent() INTO pSemesterID; END IF;
    SELECT FacultyID FROM teachers WHERE teachers.ExternalID = pExternalTeacherID INTO fid;
    SELECT x.Position
    FROM (SELECT
            teachers.ExternalID,
            teachers.FacultyID,
            @rownum := @rownum + 1 AS Position
          FROM teachers
            JOIN (SELECT @rownum := 0) r
          INNER JOIN accumulation_teachers ON teachers.ID = accumulation_teachers.TeacherID
          WHERE accumulation_teachers.SemesterID = pSemesterID AND teachers.FacultyID = fid
          ORDER BY accumulation_teachers.Rating DESC) x
    WHERE x.ExternalID = pExternalTeacherID
    INTO result;
    RETURN result;
  END //

-- ---------------------------------------------------------------------------------------------------------------------
-- Teacher_GetPositionByUniversity(pExternalTeacherID, pSemesterID)
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Teacher_GetPositionByUniversity//
CREATE FUNCTION `Teacher_GetPositionByUniversity`(
  `pExternalTeacherID` VARCHAR(255) CHARACTER SET utf8,
  `pSemesterID` INT
)
  RETURNS INT(11)
NO SQL
  BEGIN
    DECLARE result INT;
    IF pSemesterID IS NULL THEN SELECT Semester_GetCurrent() INTO pSemesterID; END IF;
    SELECT x.Position
    FROM (SELECT
            teachers.ExternalID,
            @rownum := @rownum + 1 AS Position
          FROM teachers
            JOIN (SELECT @rownum := 0) r
            INNER JOIN accumulation_teachers ON teachers.ID = accumulation_teachers.TeacherID
          WHERE accumulation_teachers.SemesterID = pSemesterID
          ORDER BY accumulation_teachers.Rating DESC) x
    WHERE x.ExternalID = pExternalTeacherID
    INTO result;
    RETURN result;
  END //

DELIMITER ;
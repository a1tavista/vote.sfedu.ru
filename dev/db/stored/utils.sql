SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

DELIMITER //

DROP FUNCTION IF EXISTS Utility_CalcMetricRating//
CREATE FUNCTION `Utility_CalcMetricRating`(
  `pMetricVotes` INT,
  `pAllVotes` INT
)
  RETURNS FLOAT
NO SQL
  BEGIN
    DECLARE sqrtRes FLOAT;
    DECLARE numerator FLOAT;

    SELECT SQRT((pMetricVotes * (pAllVotes - pMetricVotes)) / (pAllVotes) + 0.9604) INTO sqrtRes;
    SELECT ((pMetricVotes + 1.9208) / (pAllVotes) - 1.96 * sqrtRes / pAllVotes) INTO numerator;
    RETURN 5 * numerator / (1 + 3.8416 / pAllVotes);
  END //

DELIMITER ;
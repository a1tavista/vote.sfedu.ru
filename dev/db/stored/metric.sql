SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

DELIMITER //

DROP PROCEDURE IF EXISTS Metric_GetList//
CREATE PROCEDURE `Metric_GetList` () NO SQL
BEGIN
    SELECT  metrics.ID,
            metrics.Title,
            metrics.Description,
            metrics.IsVisible
        FROM `metrics`;
END //

DROP PROCEDURE IF EXISTS Metric_GetRating//
CREATE PROCEDURE `Metric_GetRating` (
    IN `pMetricID` INT,
    IN `pSemesterID` INT
) NO SQL
BEGIN
    SELECT
      teachers.ExternalID as `TeacherID`,
      evaluations.A_AvrEvaluation as `AverageEvaluation`,
      @rownum := @rownum + 1 as `RatingPosition`
    FROM evaluations
    JOIN (SELECT @rownum := 0) r
    INNER JOIN teachers ON evaluations.TeacherID = teachers.ID
    WHERE evaluations.MetricID = pMetricID AND evaluations.SemesterID = pSemesterID
    ORDER BY A_AvrEvaluation DESC;
END //

DROP PROCEDURE IF EXISTS Metric_GetTeachers//
CREATE PROCEDURE `Metric_GetTeachers` (
  IN `pMetricID` INT
) NO SQL
BEGIN
    SELECT DISTINCT teachers.ExternalID FROM teachers INNER JOIN evaluations ON teachers.ID = evaluations.TeacherID WHERE evaluations.MetricID = pMetricID;
END //

DELIMITER ;
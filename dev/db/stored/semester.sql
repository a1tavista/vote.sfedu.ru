SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

DELIMITER //
-- ---------------------------------------------------------------------------------------------------------------------
-- Semester_GetCurrent()
-- ---------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS Semester_GetCurrent//
CREATE FUNCTION `Semester_GetCurrent`()
  RETURNS INT(11)
NO SQL
  BEGIN
    DECLARE sem INT;
    SELECT semesters.ID FROM semesters ORDER BY ID DESC LIMIT 1 INTO sem;
    RETURN sem;
  END //

DELIMITER ;
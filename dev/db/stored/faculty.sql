SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

DELIMITER //

DROP PROCEDURE IF EXISTS Faculty_GetList//
CREATE PROCEDURE `Faculty_GetList` () NO SQL
BEGIN
    SELECT * FROM `faculties` ORDER BY Title ASC;
END //

DROP PROCEDURE IF EXISTS Faculty_GetRating//
CREATE PROCEDURE `Faculty_GetRating` (
    IN `pFacultyID` INT,
    IN `pSemesterID` INT
) NO SQL
BEGIN
    SELECT
      teachers.ExternalID as `TeacherID`,
      accumulation_teachers.Rating as `AverageEvaluation`,
      @rownum := @rownum + 1 as `RatingPosition`
    FROM teachers
    JOIN (SELECT @rownum := 0) r
    INNER JOIN accumulation_teachers ON accumulation_teachers.TeacherID = teachers.ID
    WHERE teachers.FacultyID = pFacultyID AND accumulation_teachers.SemesterID
    ORDER BY Rating DESC;
END //

DROP PROCEDURE IF EXISTS Faculty_GetTeachers//
CREATE PROCEDURE `Faculty_GetTeachers` (
  IN `pFacultyID` INT
) NO SQL
BEGIN
    SELECT DISTINCT teachers.ExternalID FROM teachers WHERE teachers.FacultyID = pFacultyID;
END //

DROP FUNCTION IF EXISTS Faculty_GetIDByCode//
CREATE FUNCTION `Faculty_GetIDByCode`(
  `pFacultyCode`  VARCHAR(255) CHARACTER SET utf8
)
  RETURNS INT(11)
NO SQL
  BEGIN
    DECLARE res INT;
    SELECT faculties.ID INTO res FROM faculties WHERE faculties.Code = pFacultyCode;
    IF res IS NULL THEN RETURN 404; END IF;
    RETURN res;
  END //

DELIMITER ;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

DELIMITER //

DROP PROCEDURE IF EXISTS University_GetList//
CREATE PROCEDURE `University_GetList` () NO SQL
BEGIN
    SELECT 'Южный федеральный университет';
END //

DROP PROCEDURE IF EXISTS University_GetRating//
CREATE PROCEDURE `University_GetRating` (
    IN `pUniversityID` INT,
    IN `pSemesterID` INT
) NO SQL
  BEGIN
    SELECT
      teachers.ExternalID as `TeacherID`,
      accumulation_teachers.Rating as `AverageEvaluation`,
      @rownum := @rownum + 1 as `RatingPosition`
    FROM teachers
      JOIN (SELECT @rownum := 0) r
      INNER JOIN accumulation_teachers ON accumulation_teachers.TeacherID = teachers.ID
    WHERE accumulation_teachers.SemesterID
    ORDER BY Rating DESC;
END //

DROP PROCEDURE IF EXISTS University_GetTeachers//
CREATE PROCEDURE `University_GetTeachers` (
  IN `pUniversityID` INT
) NO SQL
BEGIN
    SELECT teachers.ExternalID FROM teachers;
END //

DELIMITER ;
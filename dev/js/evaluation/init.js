$(document).ready(function () {
    // Change teacher ID when user opens evaluation dialog
    var changeTeacherID = function () {
        var teacherID = $(this).attr("data-id");
        $('#teacherID').val(teacherID);
    };
    $('.modal-trigger').leanModal();
    $('.teachers-list').on('click', '.modal-trigger', changeTeacherID);
});
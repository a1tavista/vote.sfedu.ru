function teachersListInit(year) {
    var list = new List('year-' + year, {
        valueNames: ['Name'],
        listClass: 'teachers-' + year + '-list',
        searchClass: 'teachers-' + year + '-search'
        //paginationClass: 'teachers-' + year + '-pagination',
        //page: 30,
        //plugins: [ListPagination({})]
    });
}

$(document).ready(function () {
    var Student = {
        ID: $('#id').val(),
        Key: $('#public_key').val(),
        Token: $('#token').val()
    };
    var filler = $('#evaluationDone').html();

    var list = new List('evaluated', {
        valueNames: ['Name'],
        listClass: 'evaluated-list',
        searchClass: 'evaluated-search',
        paginationClass: 'evaluated-pagination',
        page: 30,
        plugins: [ListPagination({})]
    });

    $('.tooltipped').tooltip({delay: 50});



    // Submit evaluation by clicking "Send"
    var submitEvaluation = function () {
    var metricsArr = [], metricsCount = 5;
    for (var i = 1; i <= metricsCount; i++) {
        if(document.getElementById('metric-' + i).checked)
            metricsArr[i] = i;
        document.getElementById('metric-' + i).checked = false;
    }
    var data = {
        TeacherID: $('#teacherID').val(),
        Metrics: metricsArr,
        ID: Student.ID,
        Key: Student.Key,
        Token: Student.Token
    };

    $.ajax({
        method: "POST",
        url: '/teacher/' + data.TeacherID + '/evaluate',
        dataType: "json",
        data: data,
        cache: true,
        statusCode: {
            200: function () {
                Materialize.toast('Преподаватель успешно оценен!', 3000, 'rounded');
                $('#teacher-' + data.TeacherID).children('.secondary-content').html(filler);
            },
            403: function () {
                Materialize.toast('Упс! Преподавателя нет в списке для оценивания', 3000, 'rounded')
            },
            404: function () {
                Materialize.toast('Упс! Преподаватель не найден в базе данных.', 3000, 'rounded')
            },
            406: function () {
                Materialize.toast('Упс! Преподаватель уже был оценен ранее.', 3000, 'rounded')
            },
            500: function () {
                Materialize.toast('Упс! Произошла внутренняя ошибка сервера :(', 3000, 'rounded')
            }
        }
    });
};
    $('#submit').on("click", submitEvaluation);
});
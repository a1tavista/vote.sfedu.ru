'use strict';
var gulp = require('gulp'),
    less = require('gulp-less'),
    //htmlmin = require('gulp-htmlmin'),
    csso = require('gulp-csso'),
    minify = require('gulp-minify'),
    uglify = require('gulp-uglify');

var paths = new (function () {
    var self = this;
    self.root = '.';

    var dev = self.root + '/dev';
    var dst = self.root + '/app';
    var bower = self.root + '/bower_components';

    self.bower = bower;

    self.src = {
        css: dev + '/css/**/*.css',
        less: dev + '/less/**/*.less',
        img: dev + '/img/**/*.*',
        js: dev + '/js/**/*.js',
        bower: bower + '/**/dist/**/*.*'
    };


    self.dst = {
        css: dst + '/css/',
        less: dst + '/css/',
        img: dst + '/img/',
        js: dst + '/js/',
        bower: dst + '/plugins/'
    };
})();

gulp.task('less', function() {
    gulp.src(paths.src.less)
        .pipe(less())
        .pipe(csso())
        .pipe(gulp.dest(paths.dst.less));

});

gulp.task('copy.css', function() {
    gulp.src(paths.src.css)
        .pipe(csso())
        .pipe(gulp.dest(paths.dst.css))
});

gulp.task('copy.img', function() {
    gulp.src(paths.src.img)
        .pipe(gulp.dest(paths.dst.img))
});

gulp.task('copy.plugins', function() {
    gulp.src(paths.src.bower)
        .pipe(gulp.dest(paths.dst.bower));
    gulp.src(paths.bower + '/underscore/*.js')
        .pipe(gulp.dest(paths.dst.bower + '/underscore/'));
});

gulp.task('copy.js', function() {
    console.log(paths.src.js);
    console.log(paths.dst.js);
    gulp.src(paths.src.js)
        .pipe(uglify())
        .pipe(gulp.dest(paths.dst.js))
});

gulp.task('default', function(){
    gulp.run('less', 'copy.css', 'copy.plugins', 'copy.js');
});

gulp.task('watch', function(){
    gulp.run('default');

    gulp.watch(paths.src.css, function(event) {
        gulp.run('copy.css');
    });

    gulp.watch(paths.src.js, function(event) {
        gulp.run('copy.js');
    });
});